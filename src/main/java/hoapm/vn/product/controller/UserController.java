package hoapm.vn.product.controller;

import hoapm.vn.product.dto.ProducDTO;
import hoapm.vn.product.dto.UsersDTO;
import hoapm.vn.product.mapper.IUserMapper;
import hoapm.vn.product.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }
    @PostMapping()
    public ResponseEntity<?> insert(@RequestBody UsersDTO usersDTO) {
        this.userService.upsert(usersDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAll() {
        List<UsersDTO> result = this.userService.getAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
