package hoapm.vn.product.repository;

import hoapm.vn.product.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsersRepository extends JpaRepository<UserEntity,Long> {
}
