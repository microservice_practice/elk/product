package hoapm.vn.product.mapper;

import hoapm.vn.product.dto.OrdersDTO;
import hoapm.vn.product.entity.OrderEntity;

public interface IOrderMapper extends IGenericMapper<OrdersDTO, OrderEntity> {
}
