package hoapm.vn.product.mapper;

import hoapm.vn.product.dto.UsersDTO;
import hoapm.vn.product.entity.UserEntity;

public interface IUserMapper extends IGenericMapper<UsersDTO, UserEntity> {
}
