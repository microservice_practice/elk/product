package hoapm.vn.product.controller;

import hoapm.vn.product.dto.ProducDTO;
import hoapm.vn.product.mapper.IProductMapper;
import hoapm.vn.product.service.IProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    private final IProductService productService;


    public ProductController(IProductService productService) {
        this.productService = productService;
    }
    @PostMapping()
    public ResponseEntity<?> insert(@RequestBody ProducDTO productDto) {
        this.productService.upsert(productDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAll() {
        List<ProducDTO> result = this.productService.getAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        ProducDTO result = this.productService.getById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
