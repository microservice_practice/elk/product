package hoapm.vn.product.repository;

import hoapm.vn.product.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrdersRepository extends JpaRepository<OrderEntity,Long> {
}
