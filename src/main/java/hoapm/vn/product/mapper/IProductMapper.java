package hoapm.vn.product.mapper;

import hoapm.vn.product.dto.ProducDTO;
import hoapm.vn.product.entity.ProductEntity;

public interface IProductMapper extends IGenericMapper <ProducDTO, ProductEntity> {
}
