package hoapm.vn.product.service;

import hoapm.vn.product.dto.UsersDTO;
import hoapm.vn.product.entity.UserEntity;

public interface IUserService extends IGenericService<UsersDTO,Long> {
}
