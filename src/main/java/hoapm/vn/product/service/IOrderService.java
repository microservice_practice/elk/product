package hoapm.vn.product.service;

import hoapm.vn.product.dto.OrdersDTO;
import hoapm.vn.product.entity.OrderEntity;

public interface IOrderService extends IGenericService<OrdersDTO, Long> {
}
