package hoapm.vn.product.service;

import hoapm.vn.product.dto.ProducDTO;
import hoapm.vn.product.entity.ProductEntity;

public interface IProductService extends IGenericService<ProducDTO, Long> {

}
